#!/usr/bin/perl

package ConfigFile;

BEGIN {
	use Exporter();
	@ISA = "Exporter";
	@EXPORT = qw(&getGpgPath &getLogPath &getOutPath);
}

#my $gpgPath = "/var/mail/ERCMailer";
my $gpgPath = "/home/koi8-r/prog/perl/ercmailer/";
my $logPath = $gpgPath;
my $outPath = $gpgPath;
my $pubPath = "/mnt/titan-tmp/";
my $countersPath = $gpgPath;
my $spoolPath = $gpgPath;
my $debug = 0;

sub getDebugFlag {
	return $debug;
}

sub getGpgPath {
	return $gpgPath;
}

sub getLogPath {
	return $logPath;
}

sub getOutPath {
	return $outPath;
}

sub getPubPath {
	return $pubPath;
}

sub getCountersPath {
	return $countersPath;
}

sub getSpoolPath {
	return $spoolPath;
}

1;
END{}
