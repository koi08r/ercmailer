#!/usr/bin/perl

#
# Author: koi8-r <koi08r@gmail.com>;
# Language: RU;
# Comment charset: UTF-8;
#
#
#
#

#use lib '/home/koi8-r/prog/perl/ricmailer-svn/ricmailer/project/Parser.pm';
#BEGIN {
	#unshift @INC, '.';
#}
#print join "\n", @INC; print "\n";



#use Parser;
use MIME::Parser;

use Encode qw(encode decode);
use Scalar::Util;
use HTML::FormatText();

use Reestr;
use Schetchik;

use Loging;
use JabberSend;
use SendMail;

use utf8;


use constant FALSE => 0;
use constant TRUE => 1;

# GLOBAL:
#
# Коды ошибок для демона fetchamil.
my $fmerror = 550;
my $fmsuccess = 0;
my $fmstatus = $fmerror;
use constant RECURSIVE_MAX => 99;
# Строка сообщения для Jabber.
my $xmpp_msg = "";

# Отправитель как первый параметр запуска скрипта.
my $Sender = $ARGV[0] || "";
my $Receiver = $ARGV[1] || "";


# GLOBAL:
#
my $Subject = '';
my $To = '';
my $From = '';
# Метаданные верхнего уровня (все, что нужно для успешной распаковки), могут перекрываться во вложенных частях.
my $Charset = '';
# Метаданные заголовков.
my $MimeType = '';
my $MimeEncoding = '';
my $Filename = '';

# Индикатор, что показания счетчиков удачно приняты, нужно для multipart/alternative.
my $counter_received = FALSE;

# Счетчик рекурсий.
my $recursive = 0;

# undef $/;	# Прочитать стандартный ввод как одну большую строку, а не построчно.
$|++;		# Flush STDOUT.
binmode STDOUT, ":utf8";

exit($fmerror) if( ! LogOpen() );

my $datetime = &getDateTime();

&logFileJabberDup("------------------------------------------------");
&logFileJabberDup("+ [$datetime] Получено письмо от: <$Sender> для <$Receiver>.");

#Loging::LogWrite("- Не могу подключиться к Jabber серверу.") if( ! JabberSend::jbConnect() );
LogWrite("- Не могу подключиться к Jabber серверу.") if( ! jbConnect() );

my $Parser = new MIME::Parser;

# Не нужно создавать никакие файлы.
$Parser->output_to_core(1);

# Декодировать заголовки.
#$Parser->decode_headers(1);

my $fileData = '';
#my @fileData = ();

# Чтение всего письма за раз в переменную.
{
	local $/;
#	$fileData = <STDIN>;
}

# Построчное чтение письма.
#push (@fileData, $_) while (<STDIN>);

# Сохраняем копию в tmp.
#if( open(MTMP, ">/tmp/ercmail.bin") )
#{
#	binmode(MTMP);
#	print MTMP $fileData;
#	close(MTMP);
#}

binmode STDIN;
my $Entity = $Parser->parse(\*STDIN);
#my $Entity = $Parser->parse_data( join('',@fileData) );
#my $Entity = $Parser->parse_data(\$fileData);

my $error = ($@ || $Parser->last_error);

# application/x-unparseable-multipart - Заглушка для mime неккоректных почтовых сообщений (МРЦ без вложения).
# Часть ответсвенного кода в Parser.pm:
#		### Get the boundaries for the parts:
#		my $bound = $head->multipart_boundary;
#		if (!defined($bound) || ($bound =~ /[\r\n]/)) {
#			$self->error("multipart boundary is missing, or contains CR or LF\n");
#			$ent->effective_type("application/x-unparseable-multipart");
#			return $self->process_singlepart($in, $rdr, $ent);
#		}
undef $error if ( $Entity && ($Entity->effective_type eq 'application/x-unparseable-multipart') );

if ($error)
{
	&logFileJabberDup("- Критическая ошибка парсера почтового сообщения:\n$error\n.\n");
}
else
{
	# Почтовое сообщение успешно разобрано.
	# Начинаем распаковывать.

	# Разбираем заголовок.
	my $Header  = $Entity->head;
		# Метаданные верхнего уровня.
		$Subject 		= $Header->get('Subject');						chomp($Subject);
		$To      		= $Header->get('To');							chomp($To);
		$From    		= $Header->get('From');							chomp($From);
		$Charset 		= $Header->mime_attr('content-type.charset');	chomp($Charset) if ($Charset);
		#	$Charset =~ s/=/-/g; # МРЦ "Windows=1251" хак.

		$Subject	= decode('MIME-Header', $Subject);
		$To			= decode('MIME-Header', $To);
		$From		= decode('MIME-Header', $From);

	&logFileJabberDup("+ Разбор сообщения:");
		&logFileJabberDup("++ Тема: $Subject");
		&logFileJabberDup("++ От: $From");
		&logFileJabberDup("++ Кому: $To");

		# Определяем отправителя если его адрес не передан в параметрах.
		$Sender = $1 if ( (!$Sender) && ($From =~ /<(.+)>/) );
		&logFileJabberDup("- Не получилось определить отправителя сообщения.\n") unless ($Sender);

	$fmstatus = $fmsuccess if ($Sender && &parseEntity($Entity));
	&logFileJabberDup("+ Реальный результат разбора сообщения: $fmstatus");
}

jbSend("ЕРЦ MDA, отчет по работе.", $xmpp_msg);
jbDisconnect();
Loging::LogClose();

# Заглушка: в случае ошибки fetchmail не будет бесконечно принимать одно и то же письмо.
# TODO: Исправить Reestr.pm, что бы он не выдавал ошибки, вынуждающие fetchmail принимать почту бесконечно и убрать заглушку.
$fmstatus = $fmsuccess;
exit($fmstatus);


# parseEntity:
# IN: MIME::Entity.
# OUT: FALSE если неудача, TRUE если удача.
#
sub parseEntity {
	my $entity = shift;
	my $header = $entity->head;
	my $result = FALSE;

	if($recursive > RECURSIVE_MAX) {
		&logFileJabberDup("-- Превышен показатель счетчика рекурсий для parseEntity(): ".++$recursive);
		return FALSE;
	} else {
		&logFileJabberDup("++ Показатель счетчика рекурсий parseEntity(): ".++$recursive);
	}

	# Перекрывает ли текущая кодировка вышестоящую ?
	my $tmpCharset;
	my $curCharset = $header->mime_attr('content-type.charset');
	$curCharset = $Charset if (!$curCharset || (length($curCharset) <= 0));

	$MimeType = $entity->effective_type;
	$MimeEncoding = $header->mime_encoding;
	$Filename = decode('MIME-Header', $header->recommended_filename) || "";

	# Односоставное сообщение.
	if (! $entity->is_multipart) {
		# Парсим тело сообщения.
		$tmpCharset = $Charset;
		$Charset = $curCharset;
			$result = & parseBody($entity->bodyhandle);
		$Charset = $tmpCharset;
	}
	# Многосоставное ссобщение (MIME-Type: multipart/*).
	elsif ( ($entity->parts) && ($entity->parts > 0) ) {
		# Проходим по частям.
		for (my $i=0; $i<$entity->parts; $i++) {
			# Нам важно сохранить вышестоящую кодировку в контексте текущего вызова.
			# Так как во вложенных элементах она м.б. не указана.
			$tmpCharset = $Charset;
			$Charset = $curCharset;
				# !!! РЕКУРСИЯ !!!
				$result = &parseEntity($entity->parts($i));
			$Charset = $tmpCharset;
		}
	}
	else {
	# Не удалось определить многосоставность сообщения.
		&logFileJabberDup("-- Сообщение не содержит частей.");
		return FALSE;
	}

return $result;
}


# parseBody:
# IN: MIME::Body.
# OUT: TRUE если неудача, FALSE если удача.
#
sub parseBody {
	my $body = shift;
	my $result = TRUE;
	my $Reestr = new Reestr($Sender);

	# Почтовый адрес предоставляет реестры.
	if ($Reestr->ready) {
		&logFileJabberDup("+++++ Почтовый адрес ($Sender) предоставляет реестры.");
		# Сообщение содержит прикрепленный файл.
		if ($Filename && $Reestr->setFileName($Filename)) {
			# Получаем IO::Handle тела сообщения.
			$body->binmode(1);
			my $io = $body->open('r');
			$result = $Reestr->detachAttach($io);
			foreach ($Reestr->getMessages) { &logFileJabberDup("+++++ $_") };
			&logFileJabberDup("+++++ Возникла ошибка во время сохранения почтового вложения ($Filename)")
				unless ($result);
			$io->close;
		} else {
			&logFileJabberDup("+++++ Пропускается почтовое вложение с нераспознанным именем файла ($Filename).");
		}
	# Текстовая составляющая не от поставщиков, видимо показания.
	} elsif ($MimeType =~ m/^text\/.*/) {
		&logFileJabberDup("+++++ Почтовый адрес ($Sender) не предоставляет реестры и содержит текстовые данные, видимо показания.");
		if ($counter_received) {
			&logFileJabberDup("+++++ Показания уже приняты.");
			return TRUE;
		}
		my $txt = $body->as_string;
		$txt = HTML::FormatText->format_string($txt, leftmargin => 0) if ($MimeType eq 'text/html');
		# Перекодируем кодовую страницу если нужно.
		if ($Charset) {
			$Charset =~ s/windows=1251/Windows-1251/i; # МРЦ "Windows=1251" хак.
			$txt = decode($Charset, $txt);
		}
		my $sm = new SendMail;
		my $mailmsg = ""; # В конце работы будет отправлено почтовое сообщение для $Sender.
		my $mtheme = "Отчет о приеме показаний";
		&logFileJabberDup("+++++ Текстовое сообщение ($MimeType): '$txt'.");

		my $sc = new Schetchik;
		unless ($sc->Parse($Sender,$txt)) {
			&logFileJabberDup("+++++ В сообщении не найдены показания счетчика или лицевой счет.");
			$mailmsg = "В сообщении не найдены показания счетчика или лицевой счет.";
		} elsif ($sc->Open && $sc->Transact && $sc->Commit) {
				&logFileJabberDup("+++++ Показания приняты: "."ЛС ".$sc->{ls}." ГВ ".$sc->{gv}.".");
				$mailmsg = "Ваши показания приняты: "."ЛС ".$sc->{ls}." ГВ ".$sc->{gv}.".";
				$counter_received = TRUE;
		} else {
			&logFileJabberDup("----- Не удалось сохранить показния\n");
			$mailmsg = "Не удалось сохранить показния.";
			$result = FALSE;
			$counter_received = TRUE;
		}

		unless ($sm->send($Sender, $mtheme, $mailmsg)) {
			# $result = FALSE;
			&logFileJabberDup("---- Не могу отправить почтовое сообщение.");
			# $sc->Rollback;
		}
	} else {
		# Игнорируем.
		&logFileJabberDup("++++ Игнорируемые данные.");
	}

return $result;
}


# getDateTime: Получаем текущее время.
# IN:.
# OUT: Удобночитаемая строка с датой и временем.
#
sub getDateTime()
{
	my @dt = localtime( time () );
	my ($sec, $min, $hour, $day, $month, $year) = @dt[0,1,2,3,4,5];

	# Корректируем для "нашего представления".
	$month++; $year += 1900;

return "$hour:$min:$sec $day-$month-$year";
}


# logFileJabberDup: Дублирует сообщение в лог файл и jabber сообщение.
# IN:. Сообщение.
# OUT:.
#
sub logFileJabberDup()
{
	my ($msg) = shift;

#	Loging::LogWrite($msg);
	LogWrite($msg);
	$xmpp_msg .= "$msg\n";
	print "$msg\n";
}
