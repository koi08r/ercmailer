#!/usr/bin/perl

# Require ubuntu libnet-xmpp-perl

package JabberSend;
BEGIN {
	use Exporter ();
	@ISA = "Exporter";
	@EXPORT = qw (jbConnect jbDisconnect  jbSend);
}

use Net::XMPP;
use utf8;


# Глобальные переменные.
#
my $jsconnection = undef;
my $jsserver = "192.168.2.27";
my $jsport = 5222;
my $jscomponentname = "goodwin";
my $jsusername = "ercmailer";
my $jspassword = "fetchmail";
my $jsresource = $0;

#my $xmpp_msg = "";

@jsreceiver = (
	"koi8-r\@goodwin",
);


# jbConnect: Подключается к Jabber серверу.
# IN:.
# OUT: 0 если неудача, 1 если удача.
#
sub jbConnect () {

	# !!! ЗАГЛУШКА !!!
	return 1;

	$jsconnection = new Net::XMPP::Client();

	my $status = $jsconnection->Connect (
		hostname => $jsserver,
		port => $jsport,
		componentname => $jscomponentname,
	);

	if (!(defined ($status))) {
		return 0;
	}
	else
	{
		#my $sid = $jsconnection->{SESSION}->{id};
		#$jsconnection->{STREAM}->{SIDS}->{$sid}->{hostname} = $jscomponentname;

		my @result = $jsconnection->AuthSend (
			username => $jsusername,
			password => $jspassword,
			resource => $jsresource
		);

		if ($result[0] ne "ok")
		{
				$jsconnection->Disconnect();
				return 0;
		}
		else
		{
			return 1;
		}
	}

return 0;
}


# jbSend: Шлет сообщение через Jabber сервер.
# IN:  Тема,
#      Сообщение.
# OUT: 0 если неудача, 1 если удача.
#
#sub jbSend ($$) {
sub jbSend {

	my $subject = undef;
	my $message = undef;

	return 0 if (! @_);

	if( int(@_) == 1)
	{
		$subject = "Без темы";
		$message = shift;	
	}
	else
	{
		$subject = shift;
		$message = shift;	
	}


	if ( ($jsconnection) && ($jsconnection->Connected()) )
	{
		foreach my $r (@jsreceiver)
		{
			$jsconnection->MessageSend (
				type => "normal",
				to => $r,
				#from => $jsusername,
				subject => $subject,
				body => $message
			);
			#printf "Error code: " . $jsconnection->GetErrorCode() . "\n";
		}
	} else {
		return 0;
	}
	
	return 1;
}


# jbDisconnect: Отключается от Jabber сервера.
# IN:.
# OUT:.
#
sub jbDisconnect () {
	if ($jsconnection)
	{
		$jsconnection->Disconnect ();
	}
}


return 1;

END {}


