#!/usr/bin/perl

package Loging;
BEGIN {
	use Exporter();
	@ISA = "Exporter";
	@EXPORT = qw(&LogOpen &LogWrite &LogClose);
}

use File::Path qw/make_path/;
use ConfigFile;
use utf8;

# Глобальные переменные.
#
my $loghandle = undef;
my $logfile = undef;


# LogOpen: Открываем логфайл.
# IN:  путь до логфайла, если undef то логфайл создается в соответсвии с настройками в ConfigFile.pm.
# OUT: 0 если неудача, 1 если удача.
#
sub LogOpen() {
	my ($f) = shift;

	if($f) {
		$logfile = $f;
	}
	else {
		$logfile = ConfigFile::getLogPath."/ERCMailer.log";
	}

	unless (&mkDir(ConfigFile::getLogPath)) {
		print STDERR "Can't make dir: ".ConfigFile::getLogPath.", error message '$!'\n";
		$loghandle = *STDERR;
		return 0
	}

	unless (open(LOG,">> $logfile")) {
		print STDERR "Can't open logfile: $logfile, error message '$!'\n";
		$loghandle = *STDERR;
		return 0
	}
	else
	{
		$loghandle = *LOG;
	}

	binmode LOG, ":utf8";

return 1;
}


# LogWrite: Пишем в логфайл.
# IN:  Сообщение.
# OUT:.
#
#sub LogWrite($) {
sub LogWrite {
	my $s = shift;
	print $loghandle $s, "\n" if $loghandle;
}


# LogClose: Закрываем логфайл.
# IN:.
# OUT:.
#
sub LogClose()
{
	close $loghandle if $loghandle;
}


# mkDir: Создает каталог с маской 755 рекурсивно вверх только в случае отсутсвия такового.
# IN: Путь до места создания.
# OUT: 0 если неудача, 1 если удача или если уже есть.
#

# TODO:
# make_path('foo/bar/baz', '/zug/zwang', {
# verbose => 1,
# mode => 0711,
# });

# TODO: mkDir почти одинакова как и в Reestr.pm.

sub mkDir() {
	my $path = shift;

	unless (-e $path) {
		my $oldumask = umask 0022;
		eval { make_path($path, {error => \my $err}) };
		return 0 if ($@);
		umask $oldumask;
	}

return 1;
}

return 1;
END {}
