#!/usr/bin/perl

# TODO: В сообщении jabber ссылка с вариантами действий (загрузить или редактировать) для проверки оператором.


package Reestr;

#BEGIN
#{
#	use Exporter ();
#	@ISA = "Exporter";
#	@EXPORT = qw (& senderSearch);
#}


#use Crypt::GPG;
use IPC::Run3;
#use IO::Handle;
use IO::Pipe;
use IO::File;
use Fcntl;
use File::Copy;
use File::Path qw/make_path/;
#use File::Spec;
#use Cwd;
#use Filesys::SmbClient;
use Digest::MD5;
use Time::HiRes;
use ConfigFile;
use utf8;


#my $gpgHomeDir = cwd."/gpg/";
my $gpgHomeDir = ConfigFile::getGpgPath."/gpg/";
my $gpgPasswd = "";
my $gpgSecKey='';


# Префиксы путей для сохранения реестров.
#my $scrPath = File::Spec->rel2abs($0);
#my $archPrefix = cwd."/out/arch";
#my $pubPrefix  = cwd."/out/pub";
my $archPrefix = ConfigFile::getOutPath."/out/arch";
#my $pubPrefix  = ConfigFile::getOutPath."/out/pub";
# my $pubPrefix = "/mnt/titan-tmp/";
my $pubPrefix = ConfigFile::getPubPath;
#my $pubPrefix = "/tmp/mail/ERCMailer/out/pub";

# TODO: ERROR
# Так как эти переменные объявлены вне внутренностей класса Reestr, видимо поэтому они сохраняют свое состояние даже
# после очередного вызова new Reestr() в скриптах использующих данный модуль.
# Накопитель лог сообщений.
#my @logLines = ();
# Накопитель ошибочных строк реестра.
#my @reestrErrLines = ();
# Накопитель показаний счетчика.
#my @countersCollector = ();


# Почтовые ящики, которые присылают реестры как почтовые вложения.
my @reestrProviders =
(
	{
		name =>
		{
			en => 'KOI8-R',
			ru => 'KOI8-R',
		},
		regexp => '^koi08r\@gmail\.com$',
		fmask  => '\.txt\.pgp$',
		fname  => undef,
		fmd5  => undef,
		counters  => 0,
	},
	{
		name =>
		{
			en => 'SBRF',
			ru => 'Сберкассы',
		},
		# regexp => '^fetchmail\.erc\@gmail\.com$',
		regexp => '^money\.gkh\@gmail\.com$',
		fmask  => '^\d{8}\.txt$',
		fname  => undef,
		fmd5  => undef,
		counters  => 1,
	},
	{
		name =>
		{
			en => 'MRC',
			ru => 'МРЦ',
		},
		# regexp => '^fetchmail\.erc\@gmail\.com$',
		regexp => '^alexey\.mamaev\@mtcfinance\.ru$',
		fmask  => '^\d{8}[k|t]{1}z?\.txt$', # 20100611k.txt или 20100611t.txt, или 20100611tz.txt ... 
		fname  => undef,
		fmd5  => undef,
		counters  => 0,
	},
	{
		name =>
		{
			en => 'MSCB',
			ru => 'МСКБ',
		},
		# regexp => '^fetchmail\.erc\@gmail\.com$',
		regexp => '\@mscb\.murmansk\.ru$',
		fmask  => '\.txt\.pgp$', # *.txt.pgp
		fname  => undef,
		fmd5  => undef,
		counters  => 1,
	},
	{
		name =>
		{
			en => 'MPST',
			ru => 'Мурманский почтамт',
		},
		# regexp => '^fetchmail\.erc\@gmail\.com$',
		regexp => '^mels_murmansk\@[\w\.\-]*fsps-mo\.ru$',
		fmask  => '\.(txt|doc)$',
		fname  => undef,
		fmd5  => undef,
		counters  => 1,
	},
);


sub new {
	my $proto = shift; # Извлекаем имя класса или указатель на объект.
	my $class = ref($proto) || $proto; # Если указатель, то взять из него имя класса.

	# В конструктор м.б. сразу передан интересующий почтовый адрес.
	my $sender = shift if (@_);

# 	Инициализация.
	my $self  = {};

	# Стандартные переменные.
	$self->{NAME} = undef;
	$self->{VERSION} = undef;
	$self->{OPTIONS} = [];
	
	$self->{DEBUG} = ConfigFile::getDebugFlag; # Подробные сообщения.
	#$self->{DEBUG} = 1;
	$self->{ACTIVE} = -1; # Активный поставщик, значение по умолчанию.

	# Помним, что запись '[]' ссылка на анонимный массив обращение производиться через оператор '->' или '@{}', а '()' непосредственно массив.
	# Накопитель лог сообщений.
	$self->{logLines} = [];
	# Накопитель ошибочных строк реестра.
	$self->{reestrErrLines} = [];
	# Накопитель показаний счетчика.
	$self->{countersCollector} = [];

	# Объект создается методом “освящения” ссылки на встроенный тип данных именем класса с помощью команды bless.
	# Функция ref() возвращает для такой ссылки имя класса.
	bless($self, $class); # $self становится объектом.

	# Устанавливаем активного постащика если он передан в конструктор.
	$self->{ACTIVE} = $self->senderSearchSetActive($sender) if ($sender);

return $self;
}


# senderSearchSetActive: Поиск и активация отправителя почты в списке поставщиков реестров.
# IN: email.
# OUT: -1 если неудача, >=0 если удача.
#
sub senderSearchSetActive()
{
	my $self = shift;
	my $s = shift;
	my $i = $self->senderIndex($s);

	if ($i >= 0)
	{
		return $i if $self->senderSetActive($i);
	}

return -1;
}


# senderSearch: Поиск отправителя почты в списке поставщиков реестров.
# Возвращает индекс поставщика в массиве @reestrProviders. 
# IN: email.
# OUT: -1 если неудача, >=0 если удача.
#
sub senderIndex()
{
	my $self = shift;
	my $s = shift;

	for (my $i=0; $i < int( @reestrProviders ); $i++)
	{
		my $r = $reestrProviders[$i]->{'regexp'};
		return $i if ($s =~ /$r/i);
	}

return -1;
}


# senderSetActive: Делает активным поставщика с индексом в массиве.
# IN: Индекс в массиве @reestrProviders.
# OUT: 0 если неудача, 1 если удача.
#
sub senderSetActive()
{
	my $self = shift;
	my $i = shift;

	return 0 if (! $self->senderChkIndex($i) );

	$self->{ACTIVE} = $i;

return 1;
}


# senderChkIndex: Проверяет помещается ли индекс в массив поставщиков реестров.
# IN: Индекс.
# OUT: 0 если неудача, 1 если удача.
#
sub senderChkIndex()
{
	my $self = shift;
	my $i = shift;

	if ($i > $#reestrProviders)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}


# TODO: М.б. совместить с senderSetActive().
# senderGetActive: Возвращает индекс активного поставщика.
# IN:.
# OUT: Индекс.
#
sub senderGetActive()
{
	#my $self = shift;
return shift->{ACTIVE};
}


# ready: Готовы к работе ?
# IN:.
# OUT: 0 если нет, 1 если да.
#
sub ready()
{
	#my $self = shift;
return 1 if (shift->{ACTIVE} >= 0);
return 0;
}


# setEmail: Поиск и активация отправителя почты в списке поставщиков реестров (обертка).
# IN: email.
# OUT: 0 если неудача, 1 если удача.
#
sub setEmail()
{
	my $self = shift;
	my $s = shift;
return 1 if ($self->senderSearchSetActive($s) >=0);
return 0;
}


# getEmail: Возвращает почту активного поставщика.
# IN:.
# OUT: Имя или undef.
#
sub getEmail()
{
	my $self = shift;
	
	return undef if (! $self->ready);

	my $a = $self->{ACTIVE};

return $reestrProviders[$a]->{name}->{ru};
}


# setFileName: Проверяет с реестрами ли приклепленный файл и устанавливает соответствующую переменную имени файла.
# IN: Имя файла.
# OUT: OUT: 0 если файл с неизвестным именем, 1 если удача.
#
sub setFileName()
{
	my $self = shift;
	my $fname = shift;

	return 0 if (! $self->ready);

	my $r = $reestrProviders[$self->senderGetActive]->{'fmask'};
	if ($fname =~ /$r/i)
	{
		$reestrProviders[$self->senderGetActive]->{'fname'} = $fname;
		return 1;
	}

return 0;
}


# mkDir: Создает каталог с маской 755 рекурсивно вверх только в случае отсутсвия такового.
# IN: Путь до места создания.
# OUT: 0 если неудача, 1 если удача или если уже есть.
#
sub mkDir()
{
	my $self = shift;
	my $path = shift;

	unless (-e $path) {
		my $oldumask = umask 0022;
		eval { make_path($path, {error => \my $err}) };
		return 0 if ($@);
		umask $oldumask;
	}

return 1;
}


# detachAttach: Читет IO::Handle, распаковывает gpg/pgp и пишет в соответствующий файл в каталоге arch/pub, в соответствии с setFileName().
# IN: IO::Handle.
# OUT: 0 если неудача, 1 если удача.
#
sub detachAttach()
{
	my $self = shift;
	my $io = shift;
	my $crypted = 0; # Индикатор "зашифрованости" входного файла.

	# Ничего не делаем если нет готовности к отсоединению вложения.
	return 0 if ( (! $self->ready) || (! $reestrProviders[$self->senderGetActive]->{'fname'}) || (! $io) );

	# Определяем каталоги назначения,
	# и создаем в случае их отсутсвия.
	my $archDestDir = $archPrefix. '/'. $reestrProviders[$self->senderGetActive]->{'name'}->{'en'};
		$self->mkDir($archDestDir) or return 0; # Обязательно д.быть (архивная копия).
	my $pubDestDir  = $pubPrefix.  '/'. $reestrProviders[$self->senderGetActive]->{'name'}->{'en'};
		$self->mkDir($pubDestDir); # Необязательно.

	# Определяем полный путь до файла архивной/публичной копии.
	my $fArchPath = $archDestDir. '/'. $reestrProviders[$self->senderGetActive]->{'fname'};
	my $fPubPath  = $pubDestDir.  '/'. $reestrProviders[$self->senderGetActive]->{'fname'};

	# Определяем зашифрован ли файл.
	if ($fArchPath =~ /\.(pgp|gpg)$/i )
	{
		# Обрезаем расширение pgp/gpg.
		$fArchPath =~ s/\.(pgp|gpg)$//i;
		$fPubPath  =~ s/\.(pgp|gpg)$//i;
		# Выставляем индикатор "зашифроанности".
		$crypted = 1;
	}

	# Проверяем существование одноименных файлов и генерируем случайное имя.
	#$fArchPath .= '.'.$self->randNums() while (-e $fArchPath);	$fPubPath  .= '.'.$self->randNums() while (-e $fPubPath);
	my $ext = "";
	while (-e $fArchPath) {
		$ext = $self->randNums();
		$fArchPath .= ".$ext";
	}
	$fPubPath .= $ext;

	# Открываем архивный файл для записи в него.
	$self->debug("Пытаюсь открыть $fArchPath для записи.");
	my $fArchH = IO::File->new($fArchPath, '>' )
		or return 0;
 	$fArchH->binmode;
	# Сбрасываем CloseOnExec флаг, иначе run3 не сможет прочитать пароль.
	fcntl($fArchH, F_SETFD, 0);
 	$self->debug("Удачное открытие $fArchPath для записи.");

	# Открываем файл с паролем к gpg ключу.
	my $hKey = undef;
	if($crypted)
	{
		$hKey = IO::File->new(ConfigFile::getGpgPath.'/gpg.key', '<' );
		unless ($hKey)
		{
			$self->debug("Не удалось открыть файл с паролем к GPG ключу ($!).");
			$fArchH->close;
			return 0;
		}
		# Сбрасываем CloseOnExec флаг, иначе run3 не сможет прочитать пароль.
		fcntl($hKey, F_SETFD, 0);
		$self->debug("Удачное открытие файла с паролем к GPG ключу.");
	}

	# Подсчет MD5 суммы выходного файла.
	my $md5Ctx = Digest::MD5->new;

	# Прокси pipe для дескрипторов файлов (между чтением из $io и записью в $fArchH).
	my $hProxy = IO::Pipe->new();

		# Родительский процесс.
		#
		if(my $pid = fork())
		{
			$hProxy->reader();
			$hKey->close if($crypted);

			my $r = 0;
			#sleep(3);
			$self->debug("Начало чтения данных от потомка:");
			while (<$hProxy>)
			{
				last unless ($r = print $fArchH $_);
				$self->debug("Получено от потомка: $_.");
				# Проверка корректности ЛС в реестрах, а так же
				# получение счетчиков из реестра.
				$self->getCounters($fArchPath,$_);
				$md5Ctx->add($_); # Обновляем md5.
			}
			$fArchH->close;

			waitpid($pid, 0); # Возвращает "Exit code" в переменной $?.
			$self->debug("Потомок вернул код: $?.");

			my $debugtmp = 0;
			$debugtmp = 1 if($hProxy->eof);
			if ( !$r or !$hProxy->eof or ($? != 0) )
			{
				$self->debug("Не удалось распаковать вложение: \$r == $r; \$hProxy->eof == $debugtmp; \$? == $?.");
				# Удаление при неудачной распаковке вложения.
				unlink $fArchPath if (-f $fArchPath);
				return 0;
			}
			$self->debug("Удачная распаковка вложения: \$r == $r; \$hProxy->eof == $debugtmp; \$? == $?; \$! == \'$!\'.");
		}
		# Процесс - потомок.
		#
		elsif(defined $pid)
		{
			$hProxy->writer();
			$fArchH->close;

			my $rfr = 1; # Индикатор ошибки чтения данных.

			if($crypted)
			{
				$hGpgLog = IO::File->new('/tmp/ercmailer-gpg.log', '>' ); # TODO: Что то делать в случае неудачи, хотя бы debug сообщение.
				# Вызов внешней программы gpg с перенаправлением стандартных дескрипторов.
				IPC::Run3::run3( [ 'gpg', '--homedir', $gpgHomeDir, '--batch', '--passphrase-fd', $hKey->fileno ],
					# Ссылка на анонимную функцию, отвечающую за подачу данных в stdin run3() функции.
					sub
					{
						return undef if ($io->eof); # undef для run3() означает eof.
						if(my $r = <$io>)
						{
							$rfr = 0; # Удачное чтение.
							return $r; # Возвращаем порцию данных.
						}
						else
						{
							$rfr = 1; # Ошибка чтения.
							return undef;
						}
					},
					$hProxy, $hGpgLog );

$hGpgLog->close;
				#binmode STDOUT, ":utf8"; # Почему то вызов run3() сбрасывает предыдущее значение.
				$hKey->close;
				($rfr) ? exit(-1) : exit($?);
			}
			else
			{
				$rfr = 1;
				while (<$io>)
				{
					$rfr = print $hProxy $_;
					last unless $rfr;
				}
				(!$rfr or !$io->eof) ? exit(-1): exit(0);
			}

		}

	# Сохраняем информацию об ошибочных записях реестра и собранные показания счетчиков.
	$self->saveReestrErrLines($fArchPath.".ошибки");
	$self->saveCountersCollector($fArchPath.".счетчики");

	# Копируем архивную копию в публичную.
	{
		my $oldumask = umask 0022;
		my $msg = "";
		$msg = "Копирую файл '$fArchPath' в '$fPubPath' ... ";
			if( copy($fArchPath, $fPubPath) )	{ $msg .= "Ок."; }
			else 								{ $msg .= "Ошибка."; }
		$self->pushMessage($msg);
		if (@{$self->{reestrErrLines}} > 0) {
			$msg = "Копирую файл '$fArchPath.ошибки' в '$fPubPath.ошибки' ... ";
				if( copy($fArchPath.".ошибки", $fPubPath.".ошибки") )	{ $msg .= "Ок."; }
				else 								{ $msg .= "Ошибка."; }
			$self->pushMessage($msg);
		}
		#elsif (@{$self->{countersCollector}} > 0) {
		if (@{$self->{countersCollector}} > 0) { # Получаем счетчики даже если были ошибки.
			$msg = "Копирую файл '$fArchPath.счетчики' в '$fPubPath.счетчики' ... ";
				if( copy($fArchPath.".счетчики", $fPubPath.".счетчики") )	{ $msg .= "Ок."; }
				else 								{ $msg .= "Ошибка."; }
			$self->pushMessage($msg);
		}
		umask $oldumask;
	}

	# Сохраняем доп. инф. (контрольную сумму) о сохраненном файле.
	my $md5 = $md5Ctx->hexdigest;
	$reestrProviders[$self->senderGetActive]->{'fmd5'} = $md5;
	$self->pushMessage("MD5 сумма файла: $md5");

return 1;
}


# 0 - ошибка, 1 - хорошо.
sub saveReestrErrLines() {
	my $self = shift;
	my $fReestrErrLinesH = undef;
	my $path = shift;

	return 1 unless (@{$self->{reestrErrLines}} > 0); # Нечего сохранять.

	unless ($fReestrErrLinesH = IO::File->new($path, '>' )) {
		$self->pushMessage("Не могу открыть '$path' для записи/добавления\n");
		return 0;
	}
 	$fReestrErrLinesH->binmode;
 
 	foreach (@{$self->{reestrErrLines}}) {
		unless (print $fReestrErrLinesH "$_\n") {
			$self->pushMessage("Не могу произвести запись в $path\n");
			return 0;
		}
 	}

	$fReestrErrLinesH->close;

return 1;
}


# 0 - ошибка, 1 - хорошо.
sub saveCountersCollector() {
	my $self = shift;
	my $fCountersCollectorH = undef;
	my $path = shift;

	return 1 unless (@{$self->{countersCollector}} > 0); # Нечего сохранять.
	# return 1 if (@{$self->{reestrErrLines}} > 0); # Реестр содержит ошибки.

	unless ($fCountersCollectorH = IO::File->new($path, '>' )) {
		$self->pushMessage("Не могу открыть '$path' для записи/добавления\n");
		return 0;
	}
 	$fCountersCollectorH->binmode;
 
 	foreach (@{$self->{countersCollector}}) {
		unless (print $fCountersCollectorH "$_\n") {
			$self->pushMessage("Не могу произвести запись в $path\n");
			return 0;
		}
 	}

	$fCountersCollectorH->close;

return 1;	
}


sub randNums()
{
	my $t = Time::HiRes::gettimeofday;
	$t =~ s/[^\d]//g;
	my $r = rand(10);
	$r =~ s/[^\d]//g;

return "$t$r";
}


sub getMessages()
{
	return @{ shift->{logLines} };
}

sub pushMessage()
{
	my $self = shift;

	my $msg = shift;
	if ($msg)
	{
		push @{ $self->{logLines} }, $msg;
	}
}


# Функция не используется.
#
# chkMpstLine: Проверка корректности ЛС в реестрах главпочтапта.
# IN: Путь до места хранения проверяемого файла, проверяемая линия.
# OUT: 0 если есть ошибки, 1 если ошибок нет.
#
sub chkMpstLine() {
	my $self = shift;
	my $fn = shift;
	my $ln = shift;

	# Нормализируем строку.
	chomp($ln);
	$ln =~ s/^\s+//; # Ведущие пробелы.
	$ln =~ s/\s+$//; # Ведомые пробелы и CR.
	# Или: $ln =~ s/(^\s+|\s+$)//g;

	return 1 if (length($ln) <= 0);

	# 18460202 01.03.2012 17:20:31 815275 5332.13  0.00  0
	# 18460301 01.03.2012 10:08:06 870038 14131.26 86.30 0
	# total 646 3244318.82
	# /^total:?[\x20\t]+(\d+)/i
	return 1 if ($ln =~ /^total/i);

	my @lns = split /[\t\x20]+/, $ln, -1;

	if ( (int(@lns) != 7) || ( $lns[3] !~ /^8\d{5}$/ ) )
	{
		$self->pushMessage("[MPST Err] Неверный формат строки или некорректный лицевой счет в файле реестра ($fn): $ln\n");
		push @{$self->{reestrErrLines}}, $ln;
		return 0;
	}

return 1;
}


# getCounters: Получение данных счетчиков из файла реестра.
# IN: Путь до места хранения проверяемого файла, проверяемая линия.
# OUT: 0 если есть ошибки, 1 если ошибок нет.
#
# TODO: Нормализация реестра (МРЦ, Сбер) к единой форме до вызова этой функции.
sub getCounters()
{

	my $self = shift;
	my $fn = shift;
	my $ln = shift;

	my @lns = ();
	
	return 1 unless ($fn =~ /\.txt(\.\d+)?$/i); # Работаем только если расширение *.txt или *.txt.123 .
	return 1 unless ( $reestrProviders[$self->senderGetActive]->{'counters'} ); # Выходим если реестр не поставляет показания счетчиков.

	# Нормализируем строку.
	chomp($ln);
	$ln =~ s/^\s+//; # Ведущие пробелы.
	$ln =~ s/\s+$//; # Ведомые пробелы и CR.
	# Или: $ln =~ s/(^\s+|\s+$)//g;

	return 1 if (length($ln) <= 0);

	# 18463501	13.08.2012	16:05:04	854509	5750.65	0.00	100
	# total 646 3244318.82
	# /^total:?[\x20\t]+(\d+)/i
	return 1 if ($ln =~ /^total/i);

	# МРЦ шлет первой строкой: money.gkh@gmail.com
	#return 1 if ($ln =~ /^money\.gkh\@gmail\.com$/i);

	# Коментарии - #
	return 1 if ($ln =~ /^#/);

	# Разбиваем на колонки сбербанковские реестры.
	if ( $reestrProviders[$self->senderGetActive]->{'name'}->{'en'} eq 'SBRF' ) {
		@lns = $self->split_sbrf($ln);
	} else {
	# Разбиваем на колонки стандартные реестры.
		@lns = split /[\t\x20]{1}/, $ln, -1;
	}

	# Реестры без счетчиков имеют 6 полей.
	if ((int(@lns) == 6) || (int(@lns) == 7)) {
		if ( $lns[3] !~ /^8\d{5}(0{4,6})?$/ ) { # 876231 или долговые л.с. 8856210000.
			$self->pushMessage("Неверный формат лицевого счета в файле реестра ($fn): '$ln'\n");
			push @{$self->{reestrErrLines}}, $ln;
			# return 0; # Продолжаем.
		}
		# Есть счетчики.
		if ( int(@lns) == 7 ) {
			$lns[6] = "0" unless ($lns[6]); # Если поле пустое.
			#if (($lns[6] !~ /^\d+([\.\,]{1}\d+)?$/)) {
			if (($lns[6] !~ /^\d+[\.\,]?\d*$/)) { # Бывает, что приходит такие числа как '10.' без дробной составляющей, но с дробным разделителем.
				$self->pushMessage("Некорректные показания счетчика в файле реестра ($fn): '$ln'\n");
				push @{$self->{reestrErrLines}}, $ln;
				# return 0; # Продолжаем, заносим некорректные данные для проверки оператором.
			} # else {
				push @{$self->{countersCollector}}, $lns[1] . "\t" . $lns[3] . "\t" . $lns[6] if ($lns[6] !~ /^0+$/); # Заносим счетчики.
			#}
		}
	} else {
		$self->pushMessage("Неверное количество полей в файле реестра ($fn): '$ln'\n");
		push @{$self->{reestrErrLines}}, $ln;
		return 0;
	}

return 1;
}


sub split_sbrf() {
	my $self = shift;
	my $ln = shift;
	my @lns = split /;/, $ln, -1;

	# Позиция 7 - пеня/счетчики;
	# позиция 9 - дата;
	# позиция 0 - иногда ФиО или л.с.;
	# позиция 2 - л.с.

	# Должно быть именно 10 колонок.
	if (@lns != 10) {
		$self->pushMessage("Неверный формат строки (количество колонок не 10): '$ln'\n");
		return ();
	}

	my @l = split /:/, $lns[7], -1;

	my $cntr = "0";
	$cntr = $l[1] if (@l > 1); # Есть счетчики.

	# Приводим к виду обычных реестров.
	return ( "", $lns[9], "", $lns[2], "", "", $cntr );
}


sub name
{
	my $self = shift;
	if (@_) { $self->{NAME} = shift }
return $self->{NAME};
}

sub version
{
	my $self = shift;
	if (@_) { $self->{VERSION} = shift }
return $self->{VERSION};
}

sub options {
	my $self = shift;
	if (@_) { @{ $self->{OPTIONS} } = @_ }
return $self->{OPTIONS};
}




sub debug()
{
	my $self = shift;
	my $msg = shift;
		$self->pushMessage($msg) if (($msg) && ($self->{DEBUG}));
		#&pushMessage($msg);
	return 1;
}


1;
END{}
