#!/usr/bin/perl

package Schetchik;

use locale;
use POSIX qw(locale_h);
setlocale(LC_ALL,"ru_RU.UTF-8");
use File::Path qw/make_path/;
use ConfigFile;
use XMLCounters;
use utf8;
use encoding 'utf8';


my $xml = undef;

# new: Конструктор.
#
sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	# Инициализация.
	my $self  = {};

	$self->{NAME} = undef;
	$self->{VERSION} = undef;
	$self->{OPTIONS} = [];
	# Накопитель лог сообщений.
	$self->{logLines} = [];

	$self->{src} = undef;
	$self->{ls} = undef;
	$self->{gv} = undef;

	bless($self, $class);

return $self;
}


# Parse: Поиск в тексте значений для лицевого счета и счетчиков и установка этих значений.
# IN: Адрес передавшего, текст сообщения.
# OUT: 0 в случае неудачи и 1 если успех.
#
sub Parse {
	my $self = shift;
	my $src = shift;
	my $text = shift;
	my ($ls,$gv) = ("","");

	$text =~ s/(?:[^\w,\.\d]+|[\n\r\s]+)/\x20/g;
	$ls = $1 if ($text =~ m/ЛС\s*(\d+)/i);
	$gv = $1 if ($text =~ m/ГВ\s*(\d+(?:[.,]\d+)?)/i);

	if ($src && $ls && $gv) {
		$self->{src} = $src; $self->{ls} = $ls; $self->{gv} = $gv;
		return 1;
	}

	$self->{src} = undef; $self->{ls} = undef; $self->{gv} = undef;
return 0;
}


# Open: Открытие xml.
# IN: .
# OUT: 0 в случае неудачи и 1 если успех.
#
sub Open {
	my $self = shift;
	my $prefix = ConfigFile::getCountersPath."/out/counters/";
	return 0 unless ($self->mkDir($prefix));
	return 0 unless($xml = XMLCounters->new("$prefix/counters.txt"));
return 1;
}


# Transact: Сохраняет показания в память xml.
# IN: src, ls, gv.
# OUT: 0 если неудача, 1 если удача.
#
sub Transact {
	my $self = shift;
	return 0 unless($xml);
	return 0 unless ($xml->transact($self->{src}, $self->{ls}, $self->{gv}));
	return 1;
}

# Transact: Сохраняет показания в файл.
# OUT: 0 если неудача, 1 если удача.
#
sub Commit {
	my $self = shift;
	return 0 unless($xml);
	return ($xml->commit);
}


# mkDir: Создает каталог с маской 755 рекурсивно вверх только в случае отсутсвия такового.
# IN: Путь до места создания.
# OUT: 0 если неудача, 1 если удача или если уже есть.
#
sub mkDir {
	my $self = shift;
	my $path = shift;

	unless (-e $path) {
		my $oldumask = umask 0022;
		eval { make_path($path, {error => \my $err}) };
		return 0 if ($@);
		umask $oldumask;
	}

return 1;
}


1;
END {}
