#!/usr/bin/perl
package SendMail;

# use MIME::Decoder;
use Encode qw/encode/;
use MIME::Entity;
use File::Path qw/make_path/;
use IO::File;
use utf8;

# new: Конструктор.
#
sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self  = {};

	$self->{NAME} = undef;
	$self->{VERSION} = undef;
	$self->{OPTIONS} = [];
	$self->{eml} = undef;

	bless($self, $class);

return $self;
}


# send: Отправить почтовое сообщение.
# IN: Адрес получателя, тема, текст сообщения.
# OUT: 0 в случае неудачи, 1 если удачно.
#
sub send {
	my $self = shift;
	my $to = shift;
	my $itheme = shift;
	my $theme = "";
	my $text = shift;

	#my $decoder = new MIME::Decoder 'base64';
	#return 0 unless ($decoder);
	#$decoder->encode($itheme, $theme);

	my $entity = MIME::Entity->build (
	    From        => 'fetchmail.erc@gmail.com',
	    To          =>  $to,
	    Subject     =>  encode('MIME-Header', $itheme),
	    Charset     => 'UTF-8',
	    Data        =>  $text,
	    Encoding    => "base64",
	    'X-Mailer'  => "ERCMailer",
	);

	my $sr;
	eval {
		$sr = $entity->send (
			'smtps',
			Server => "smtp.gmail.com",
			Auth => [ "fetchmail.erc\@gmail.com", "Hfcxtn_51" ],
			Port => 465,);
	};
	$self->{eml} = $entity->as_string;

	# if (!$sr || $@) {
	#		if ($self->mkDir(ConfigFile::getSpoolPath) && (my $fh = new IO::File ">> $file")) {
	#			$fh->binmode
	#			$entity->print(ConfigFile::getSpoolPath."/spool");
	#		}
	# }

	return 0 if (!$sr || $@);

return 1;
}


sub mkDir {
	my $self = shift;
	my $path = shift;

	unless (-e $path) {
		my $oldumask = umask 0022;
		eval { make_path($path, {error => \my $err}) };
		return 0 if ($@);
		umask $oldumask;
	}

return 1;
}


1;
END {}
