#!/usr/bin/perl

# Установка зависимостей под Ubuntu: apt-get install libdbd-sybase-perl.

package StackDB;

use IO::File;
use DBI;
use utf8;

sub new {
	my $proto = shift; # Извлекаем имя класса или указатель на объект.
	my $class = ref($proto) || $proto; # Если указатель, то взять из него имя класса.

	# Инициализация.
	my $self  = {};

	# Стандартные переменные.
	$self->{NAME} = undef;
	$self->{VERSION} = undef;
	$self->{OPTIONS} = [];

	# Аутентификационные даные для подключения к БД.
	$self->{LOGIN} = undef;
	$self->{PASSWD} = undef;

	bless($self, $class); # $self становится объектом.

return $self;
}


sub loadLoginData()
{
	my $self = shift;
	my $fname = shift;

	my $hPwd = IO::File -> new( ($fname)?$fname:"stackdb.pwd", '<' )
		or return 0;
	#$hPwd->binmode;

	while ( my $ln = <$hPwd> )
	{
		chomp $ln;
		next unless $ln;
		my ($name, $value) = split /=/, $ln, 2;
		
		if    ("\U$name" eq "LOGIN")
		{
			$self->{LOGIN}  = $value;
			last if $self->{PASSWD};
		}
		elsif ("\U$name" eq "PASSWORD")
		{
			$self->{PASSWD} = $value;
			last if $self->{LOGIN};
		}
	}

	$hPwd->close();

return 1;
}


sub test()
{
	my $self = shift;



# Connect
my ($host,$port,$database,$user,$pass) = ("192.168.2.88","1433","murmansk_severomorsk","sa","Spasw0rkcomplete_");

# Version
my $dbh = DBI -> connect("DBI:Sybase:server=$host:$port;database=$database",$user,$pass);
my $sth = $dbh -> prepare('select @@VERSION;');
$sth -> execute();
print "Database: " . $sth -> fetchrow_array();
$sth -> finish();

# Query
#my @data;
#my $sth=$dbh->prepare('sql query');
#$sth->execute();
#while ( @data = $sth->fetchrow_array() )
#{
#    my $x = @data[0];
#    print "result\t $x\n";
#}

$dbh->disconnect();

return 0;
}

sub availDrivers()
{
	foreach (DBI -> available_drivers) { print "DBI driver available: $_\n" }	
}


1;
END{}
