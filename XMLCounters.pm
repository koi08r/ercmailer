#!/usr/bin/perl

package XMLCounters;

use XML::LibXML;
use utf8;


my ($doc,$root,$fname);
# new: Конструктор.
# IN: Путь к xml файлу.
# OUT: undef в случае неудачи.
#
sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	# В конструктор д.б. сразу передан путь к xml файлу.
	return undef unless (&do(@_));

	my $self  = {};
	$self->{NAME} = undef;
	$self->{VERSION} = undef;
	$self->{OPTIONS} = [];
	$self->{logLines} = [];

	$self->{transact} = undef;

	bless($self, $class);

return $self;
}


sub do {
	$fname = shift;
	eval {
		unless (-e $fname) {
			$doc = XML::LibXML::Document->new('1.0', 'utf-8');
			$root = $doc->createElementNS('me@koi8-r.name', 'root');
			$doc->setDocumentElement($root);
		} else {
			$doc = XML::LibXML->load_xml(location => $fname, keep_blanks => 0);
			$root = $doc->getDocumentElement();
		}
	};
	if ($@) { return undef; } else { return 1 }
}


# In: Source, Ls, Gv.
sub transact {
	my $self = shift;
	my ($src,$ls,$gv) = @_;

	my $el_sc = $doc->createElement("sc");
	# $el_sc->setAttribute("datetime",main::getDateTime());
	my ($time,$date) = $self->getDateTime;
	$el_sc->setAttribute("time",$time);
	$el_sc->setAttribute("date",$date);
	$el_sc->setAttribute("src",$src);

	my $el_ls = $doc->createElement("ls");
	my $el_gv = $doc->createElement("gv");

	$el_ls->appendTextNode($ls);
	$el_gv->appendTextNode($gv);

	$el_sc->appendChild($el_ls);
	$el_sc->appendChild($el_gv);

	# TODO: Массив (или хеш с ключами UUID) транзакций.
	$self->{transact} = $el_sc;
	$root->appendChild($el_sc);

	return 1;
}


sub commit {
	my $self = shift;
	return $doc->toFile($fname,2);
}


# getDateTime: Получаем текущее время и дату.
# IN:.
# OUT: Массив из строк даты и времени.
#
sub getDateTime {
	my $self = shift;
	my @dt = localtime( time () );
	my ($sec, $min, $hour, $day, $month, $year) = @dt[0,1,2,3,4,5];
	# Корректируем для "нашего представления".
	$month++; $year += 1900;
return ("$hour:$min:$sec", "$day-$month-$year");
}


1;
END {
	# $doc->toFile($fname,2);
}
