#!/usr/bin/perl



use File::Copy;
use Time::HiRes;
use Net::XMPP;
use Net::SMTP::SSL;
use DBI;

use MIME::Parser;
use MIME::Entity;
use MIME::Body;


# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                 Global Variables                    #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #

# firebird with ls's server opts
$fbuser = "SYSDBA";
$fbpasswd = "Gw!22R";
$fbhost = "localhost";
$fbport = "3050";
$fbdialect = "3";
$db = "/db/kwrt/alfa.fdb";
$dsn = "dbi:InterBase:db=$db;host=$fbhost;port=$fbport;ib_dialect=$fbdialect";
$dbh = undef;
$sth = undef;

# external mailbox settings
$external_mail = 'money.gkh@gmail.com';
$external_passwd = 'Ht80k.wbz_';
$external_smtp = 'smtp.gmail.com';

# return to fetchamil results
$error = 550; # error deliver, return fail to fetchmail
$success = 0; # success deliver, return <ok> to fetchmai

# path to binaries
$munpack_bin = '/usr/bin/munpack';
$gpg_bin = '/usr/bin/gpg';

# gpg keys and sigs
$gpg_home = '/var/spool/mail/gpg';
$gpg_passwd = 'ERC5ign_';


# general vars
$path = '/var/spool/mail/';
$logfile = $path.'/ricmailer.log';
$MAILBOX = $path.'/unsorted.mbox';
$tmp_dir = $path.'/tmp/';

# test vars
$test_store_dir = '/tmp/';
$test_samba_dir = '/tmp/';
$test_sender = 'koi08r@gmail.com';

# MRC vars
$mrc_store_dir = '/arch/mrc/mail/';
$mrc_samba_dir = '/home/tmp/MRC/';
$mrc_sender = 'alexey.mamaev@mtcfinance.ru';

# MSCB vars
$mscb_store_dir = '/arch/mscb/mail/';
$mscb_samba_dir = '/home/tmp/MSCB/';
#$mscb_sender =  'uscheglova@mscb.murmansk.ru';
#$mscb_sender2 = 'eyakovleva@mscb.murmansk.ru';

# MPST vars (Murmansk postamp)
$mpst_store_dir = '/arch/mpst/mail/';
$mpst_samba_dir = '/home/tmp/MPST/';
#$mpst_sender = 'mels_murmansk@tehno.fsps-mo.ru';

# Jabber server settings
$xmpp_connection = undef;
$xmpp_connected = 0;
$xmpp_port = 5222;
$xmpp_server = "192.168.6.254";
$xmpp_domain = "erc51.info";
$xmpp_username = "gmailbot\@erc51.info";
@xmpp_receiver = (
	#"koi8-r\@erc51.ru",
	#"врадий\@erc51.ru",
	#"gashek\@erc51.ru",
	"koi8-r\@erc51.info",
);
$xmpp_password = "Ht3Jnu_PAWtYsaf8B6JAUBcEj";
$xmpp_resource = $0;
$xmpp_msg = "";



#######################################
# SEND EMAIL
#######################################
# Send email through $external_mail
# Input: subject,message,recipient
sub sendMAIL(){

my $msg = '';
my $sbj = '';
my $rcpt = '';

	if(! $_[2]){
		$rcpt = 'root@localhost';
	}else{
		$rcpt = $_[2];
	}


	if(! $_[0]){
		$sbj = 'No theme';
	}else
	{
		$sbj = $_[0];
		utf8::decode($sbj);
	}

	if(! $_[1]){
		$msg = 'No text';
	}else{
		$msg = $_[1];
		utf8::decode($msg);
	}

print (LOG " * try send mail to $rcpt ...\n");


$smtp = Net::SMTP::SSL->new( $external_smtp,
                             Port => 465,
                             Debug => 0
) || return("error");


$smtp->auth($external_mail, $external_passwd) || return("error");


$smtp->mail("$external_mail\n");
$smtp->to("$rcpt\n");


$smtp->data();

$smtp->datasend("From: <$external_mail>\n");
$smtp->datasend("To: <$rcpt>\n");
$smtp->datasend("Subject: $sbj\n");
$smtp->datasend("Reply-To: <$external_mail>\n");
$smtp->datasend("Content-Type: text/plain; charset=utf8\n");
$smtp->datasend("Content-Transfer-Encoding: 8bit\n");
$smtp->datasend("\n");
$smtp->datasend("$msg\n");

$smtp->dataend();

$smtp->quit;

print (LOG " * success send mail to $rcpt\n");

return("ok");
}



#######################################
# SEND JABBER MSG
#######################################
# Send message through jabber server
# Input: subject,message
sub sendXMPP(){

	my $msg = '';
	my $sbj = '';

	if ( ($xmpp_connected == 1) && ($xmpp_connection) ) {

		if(! $_[0]){
			$sbj = 'No theme';
		}else
		{
			$sbj = $_[0];
			utf8::decode($sbj);
		}

		if(! $_[1]){
			$msg = 'No text';
		}else{
			$msg = $_[1];
			utf8::decode($msg);
		}

		foreach $R (@xmpp_receiver) {
			$xmpp_connection->MessageSend (
				type => "normal",
				to => $R,
				from => $xmpp_username,
				subject=>$sbj,
				body=>$msg,
			);
		}

	}
}



# for unique file extension
sub secondExt(){
  return(Time::HiRes::gettimeofday);
}



#######################################
# CLEAN TMP
#######################################
# Delete all files(not invisible files)
# from tmp direcotory
sub cleanTmpDir{
  # try cleanup tmp dir
  if(opendir(TMP, $tmp_dir)){
    my @files = readdir(TMP);
    if(@files){
      foreach(@files){
	if(($_ ne ".")&&($_ ne "..")){ # do not touch curent dir and parent dir
	  unlink("$tmp_dir/$_")if((-f "$tmp_dir/$_")&&(-w "$tmp_dir/$_")&&(-o "$tmp_dir/$_"));
	}
      }
    }
    closedir(TMP);
  }
}



#######################################
# ERROR EXIT
#######################################
# raise error exit,
# print error message in log
# and close log handler
sub errExit(){
  print(LOG $_[0])if $_[0];
  &cleanTmpDir();
  print(LOG "------------------------------------------------\n");
  close(LOG);

  my $xmsg = '';
  $xmsg = $_[0] if ($_[0]);
  &sendXMPP("error exit", $xmsg);



 if ($xmpp_connected == 1) {
	$xmpp_connection->Disconnect();
	$xmpp_connected = 0;
 }


  exit($error);# error deliver, return fail to fetchmail
}






#######################################
# CHECK LS FILE
#######################################
# read file and checks that would
# ls from file were present in
# firebird database
sub checkLsFile(){


my $lsfile = $_[0];

print(LOG " * start checks ls on $lsfile\n");


my $mes = '';
my @lsarra = ();




# Open file
if(! open (FD, $lsfile)) {
	print(LOG " * cant open $lsfile\n");
	return("error");
}



# connect to fb server
$dbh = DBI->connect($dsn, $fbuser, $fbpasswd);

if(! $dbh) {
	print(LOG " * connection to firebird server failed\n");
	close(FD);
	return("error");
}


$sth = $dbh->prepare("SELECT LS FROM VED");
if ( (! $sth) || (! $sth->execute) ){
	$dbh->disconnect;
	print(LOG " * cant execute select on firebird server\n");
	close(FD);
	return("error");
}else{

	@lsarra = ();

	while(my @i = $sth->fetchrow_array){

		if($i[0]){
			push (@lsarra, $i[0]);
		}
			
	}

	$sth->finish();
	$dbh->disconnect;

	print (LOG " * loaded ".int(@lsarra)." ls's from firebird server\n");

}




my $ls_count = 0;
my $ls_total = 0;

while(<FD>){

	chomp;

	if ($_ =~ /^total:?[\x20\t]+(\d+)/i){
		$ls_total = $1;
	}else{

		my @ln = split(/[\t\x20]+/);


		# simple check line
		if ( (@ln < 6) || ($ln[3] !~ /\d{6}/) ) {
			$mes .= $_."\n";
		}
		# firebird check
		else {

			my $found = 0;

			foreach $n (@lsarra){
				if($n eq $ln[3]){
					$found = 1;
					last;
				}
			}

			if ($found == 0){
				$mes .= $_."\n";
			}

		}



		$ls_count++;
	}

}



# successfull exit
close(FD);

if ($mes){

	print (LOG " * result:\n$mes");
	print (LOG " * records count not match: $ls_count <> $ls_total\n") if ($ls_count != $ls_total);

	if ( $ls_count != $ls_total ){
		$mes .= "\nОбщее количество обработаных записей не соответствует заявленному\n";
	}

	$mes =
	"Здравствуйте, просьба, проверить и выслать верные данные.\n\n".$mes."\n\n".
	"--\nС уважением, ОАО \"Единый расчетный центр\" г. Мурманск.\n".
	"инженер-программист Павлов Евгений\n".
	"т. (815-37) 3-44-17";


	my $mailresult = "отправленно";
	#if ( &sendMAIL("Ошибки в реестре",$mes,$ARGV[0]) ne "ok" ){
		$mailresult = "!!! НЕ ОТПРАВЛЕНО !!!";
	#}

	&sendXMPP("ricmailer5: отчет о проверке лицевых счетов",
		"Сформировано и $mailresult следующее почтовое сообщение для $ARGV[0]\n\n".$mes);

	#print (LOG " * Попытка отправить xmpp сообщение:\nСформировано и $mailresult следующее почтовое сообщение для $ARGV[0]\n\n$mes");


}else{
	print (LOG " * errors not found\n");

	&sendXMPP("ricmailer5: отчет о проверке лицевых счетов",
		"$lsfile: ошибок не найдено");
}

return($mes);
}














# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                         EP                          #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #

&cleanTmpDir();

# get current time
$datetime = localtime (time ());

# open log file
exit($error)if(open(LOG,">> $logfile") == 0);
binmode LOG, ":utf8";
print(LOG "------------------------------------------------\n");
print(LOG " * [$datetime] start with params: $ARGV[0] $ARGV[1]\n");

$xmpp_msg .= "На $external_mail получено письмо от $ARGV[0]\n";



# connect to jabber server
$xmpp_connection = new Net::XMPP::Client();

my $status = $xmpp_connection->Connect(
	hostname => $xmpp_server,
	port => $xmpp_port,
	componentname => $xmpp_domain,
);


if (!(defined($status))) {
	print(LOG " * connection to $xmpp_server failed\n");
	$xmpp_connection = undef;
	$xmpp_connected = 0;
}else{

	my $sid = $xmpp_connection->{SESSION}->{id};
	$xmpp_connection->{STREAM}->{SIDS}->{$sid}->{hostname} = $xmpp_domain;

	my @result = $xmpp_connection->AuthSend (
		username => $xmpp_username,
		password => $xmpp_password,
		resource => $xmpp_resource
	);

	if ($result[0] ne "ok") {
		print(LOG " * authorization $xmpp_username failed\n");
		if ($xmpp_connection)
		{
			$xmpp_connection->Disconnect();
			$xmpp_connected = 0;
			$xmpp_connection = undef;
		}
	}else{
		$xmpp_connected = 1;
	}

}












# read mail from stdinput
@mail = <STDIN>;
unshift @mail, "From $ARGV[0] $datetime\n"; # qmail mbox format, rfc4155
push @mail, "\n"; # qmail mbox format, rfc4155





# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                        MRC                          #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #
if($ARGV[0] eq $mrc_sender){

  print(LOG " * mrc mail\n");
  $xmpp_msg .= "Это почта МРЦ\n";

  $MAILBOX = $path.'mrc.mbox'; # all mrc mail collect in this mailbox

  open(PIPE,"| $munpack_bin -f -C $tmp_dir/")||&errExit(" * cant open mpack pipe\n");

  print(LOG " * run($?): '$munpack_bin -f -C $tmp_dir/'\n");
  print PIPE @mail;
  close(PIPE);

  # set right mode and delete not needed files
  opendir(TMP_DIR, $tmp_dir)||&errExit(" * cant open dir $tmp_dir\n");

  my @files = readdir(TMP_DIR);
  if(@files == undef){
    closedir(TMP_DIR);
    &errExit(" * cant read dir $tmp_dir\n");
  }
  closedir(TMP_DIR);


  $xmpp_msg .= "Содержит следующие файлы:\n";

  foreach(@files){
    if(($_ ne ".")&&($_ ne "..")){ # do not touch curent dir and parent dir

	$xmpp_msg .= "$_:";

      if($_ =~ /^\d{8}[k|t]{1}z?\.txt$/i){  # we need files such as 20100611k.txt or 20100611t.txt or 20100611kz.txt or 20100611tz.txt

	my $store_dest = "$mrc_store_dir/$_";
	$store_dest .= ".".&secondExt() if(-e $store_dest);
	copy("$tmp_dir/$_",$store_dest)||&errExit(" * copy failed(utf8::decode($!)) $tmp_dir/$_ -> $store_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $store_dest\n");
	$xmpp_msg .= "скопирован в $store_dest";

	my $samba_dest = "$mrc_samba_dir/$_";
	$samba_dest .= ".".&secondExt() if(-e $samba_dest);
	copy("$tmp_dir/$_",$samba_dest)||&errExit(" * copy failed(utf8::decode($!)) $tmp_dir/$_ -> $samba_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $samba_dest\n");
	$xmpp_msg .= ",скопирован в $samba_dest";

	chmod(0644,$samba_dest)||&errExit(" * cant chmod $samba_dest\n");
	print(LOG " * chmod $samba_dest\n");
	$xmpp_msg .= "\n";

	unlink("$tmp_dir/$_");

      }else{
		unlink("$tmp_dir/$_");
		$xmpp_msg .= "удален\n";
	}

    }
  }

}





# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                       MSCB                          #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#if(($ARGV[0] eq $mscb_sender)||($ARGV[0] eq $mscb_sender2)){
if($ARGV[0] =~ /\@mscb.murmansk.ru$/){


  print(LOG " * mscb mail\n");
  $xmpp_msg .= "Это почта МСКБ\n";

  $MAILBOX = $path.'mscb.mbox'; # all mscb mail collect in this mailbox

  open(PIPE,"| $munpack_bin -f -C $tmp_dir/")||&errExit(" * cant open mpack pipe\n");
  print(LOG " * run($?): '$munpack_bin -f -C $tmp_dir/'\n");
  print PIPE @mail;
  close(PIPE);

  # set right mode and delete not needed files
  opendir(TMP_DIR, $tmp_dir)||&errExit(" * cant open dir $tmp_dir\n");

  my @files = readdir(TMP_DIR);
  if(@files == undef){
    closedir(TMP_DIR);
    &errExit(" * cant read dir $tmp_dir\n");
  }
  closedir(TMP_DIR);

  $xmpp_msg .= "Содержит следующие файлы:\n";

  foreach(@files){
    if(($_ ne ".")&&($_ ne "..")){ # do not touch curent dir and parent dir

	$xmpp_msg .= "$_:";

      #if($_ =~ /^(mscb\d{6}\.txt)\.pgp$/i){  # we need files such as mscb100804.txt.pgp
      if($_ =~ /(.*)\.txt\.pgp$/i){  # we need files such as *.txt.pgp

	open(PIPE,"| $gpg_bin --batch --yes --homedir $gpg_home --passphrase-fd 0 -o $tmp_dir/$1 -d $tmp_dir/$_")
	||&errExit(" * cant open gpg pipe\n");
	print PIPE $gpg_passwd;
	close(PIPE);
	print(LOG " * run($?): '$gpg_bin --batch --yes --homedir $gpg_home --passphrase-fd 0 -o $tmp_dir/$1 -d $tmp_dir/$_'\n");

	my $store_dest = "$mscb_store_dir/$1";
	$store_dest .= ".".&secondExt() if(-e $store_dest);
	copy("$tmp_dir/$1",$store_dest)||&errExit(" * copy failed($!) $tmp_dir/$1 -> $store_dest\n");
	print(LOG " * copy $tmp_dir/$1 -> $store_dest\n");
	$xmpp_msg .= "расшифрован в $1 и скопирован в $store_dest";

	my $samba_dest = "$mscb_samba_dir/$1";
	$samba_dest .= ".".&secondExt() if(-e $samba_dest);
	copy("$tmp_dir/$1",$samba_dest)||&errExit(" * copy failed($!) $tmp_dir/$1 -> $samba_dest\n");
	print(LOG " * copy $tmp_dir/$1 -> $samba_dest\n");
	$xmpp_msg .= ",скопирован в $samba_dest";

	chmod(0644,$samba_dest)||&errExit(" * cant chmod $samba_dest\n");
	print(LOG " * chmod $samba_dest\n");
	$xmpp_msg .= "\n";

	unlink("$tmp_dir/$1");
	unlink("$tmp_dir/$_");

      }else{
		unlink("$tmp_dir/$_");
		$xmpp_msg .= "удален\n";
	}

    }
  }

}








# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                        MPST                         #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #
if ($ARGV[0] =~ /^mels_murmansk\@[\w\.\-]*fsps-mo.ru$/i){

  print(LOG " * mpst mail\n");
  $xmpp_msg .= "Это почта Главпочтампта\n";

  $MAILBOX = $path.'mpst.mbox'; # all mpst mail collect in this mailbox

  open(PIPE,"| $munpack_bin -f -C $tmp_dir/")||&errExit(" * cant open mpack pipe\n");
  print(LOG " * run($?): '$munpack_bin -f -C $tmp_dir/'\n");
  print PIPE @mail;
  close(PIPE);

  # set right mode and delete not needed files
  opendir(TMP_DIR, $tmp_dir)||&errExit(" * cant open dir $tmp_dir\n");

  my @files = readdir(TMP_DIR);
  if(@files == undef){
    closedir(TMP_DIR);
    &errExit(" * cant read dir $tmp_dir\n");
  }
  closedir(TMP_DIR);

  $xmpp_msg .= "Содержит следующие файлы:\n";

  foreach(@files){
    if(($_ ne ".")&&($_ ne "..")){ # do not touch curent dir and parent dir

	$xmpp_msg .= "$_:";

      if(
		#($_ =~ /^post\d{8}\.txt$/i)||
		($_ =~ /\.(txt|doc)$/i)
		#($_ =~ /^\d{1,2}\,\d{1,2}\.txt$/i)||
		#($_ =~ /^(\d{1,2}[\-\,])?\d{1,2}[\,\.]\d{1,2}\.txt$/i)
	){	# we need files such as post10080434.txt or 05,10.txt, or 03-08,11
		# or 19,20.11.txt

	my $store_dest = "$mpst_store_dir/$_";
	$store_dest .= ".".&secondExt() if(-e $store_dest);
	copy("$tmp_dir/$_",$store_dest)||&errExit(" * copy failed($!) $tmp_dir/$_ -> $store_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $store_dest\n");
	$xmpp_msg .= "скопирован в $store_dest";

	my $samba_dest = "$mpst_samba_dir/$_";
	$samba_dest .= ".".&secondExt() if(-e $samba_dest);
	copy("$tmp_dir/$_",$samba_dest)||&errExit(" * copy failed($!) $tmp_dir/$_ -> $samba_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $samba_dest\n");
	$xmpp_msg .= ",скопирован в $samba_dest";

	chmod(0644,$samba_dest)||&errExit(" * cant chmod $samba_dest\n");
	print(LOG " * chmod $samba_dest\n");
	$xmpp_msg .= "\n";

	&checkLsFile("$tmp_dir/$_") if ($_ =~ /^post\d{8}\.txt$/i);

	unlink("$tmp_dir/$_");

      }else{
		unlink("$tmp_dir/$_");
		$xmpp_msg .= "удален\n";
	}

    }
  }

}




# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                       TEST                          #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #
if($ARGV[0] eq $test_sender) {

  print(LOG " * test mail\n");
  $xmpp_msg .= "Это почта Тест\n";

  $MAILBOX = $path.'test.mbox'; # all test mail collect in this mailbox

  open(PIPE,"| $munpack_bin -f -C $tmp_dir/")||&errExit(" * cant open mpack pipe\n");
  print(LOG " * run($?): '$munpack_bin -f -C $tmp_dir/'\n");
  print PIPE @mail;
  close(PIPE);

  # set right mode and delete not needed files
  opendir(TMP_DIR, $tmp_dir)||&errExit(" * cant open dir $tmp_dir\n");

  my @files = readdir(TMP_DIR);
  if(@files == undef){
    closedir(TMP_DIR);
    &errExit(" * cant read dir $tmp_dir\n");
  }
  closedir(TMP_DIR);

  $xmpp_msg .= "Содержит следующие файлы:\n";

  foreach(@files) {
    if(($_ ne ".")&&($_ ne "..")){ # do not touch curent dir and parent dir

	$xmpp_msg .= "$_:";

	if( $_ =~ /^post\d{8}\.txt$/i){

	my $store_dest = "$test_store_dir/$_";
	$store_dest .= ".".&secondExt() if(-e $store_dest);
	copy("$tmp_dir/$_",$store_dest)||&errExit(" * copy failed($!) $tmp_dir/$_ -> $store_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $store_dest\n");
	$xmpp_msg .= "скопирован в $store_dest";

	my $samba_dest = "$test_samba_dir/$_";
	$samba_dest .= ".".&secondExt() if(-e $samba_dest);
	copy("$tmp_dir/$_",$samba_dest)||&errExit(" * copy failed($!) $tmp_dir/$_ -> $samba_dest\n");
	print(LOG " * copy $tmp_dir/$_ -> $samba_dest\n");
	$xmpp_msg .= ",скопирован в $samba_dest";

	chmod(0644,$samba_dest)||&errExit(" * cant chmod $samba_dest\n");
	print(LOG " * chmod $samba_dest\n");
	$xmpp_msg .= "\n";

	&checkLsFile("$tmp_dir/$_") if ($_ =~ /^post\d{8}\.txt$/i);

	unlink("$tmp_dir/$_");

	}else{
		unlink("$tmp_dir/$_");
		$xmpp_msg .= "удален\n";
	}

    }
  }

}





# * * * * * * * * * * * * * * * * * * * * * * * * * * #
#                                                     #
#                     DELIVER                         #
#                                                     #
# * * * * * * * * * * * * * * * * * * * * * * * * * * #
open(MBOX,">> $MAILBOX")||&errExit(" * cant write to $MAILBOX\n");

# lock mbox
if( flock(MBOX, 2) == 0){
  close(MBOX);
  &errExit(" * cant lock $MAILBOX\n");
}

# Write mail to the mailbox
print MBOX @mail;

# unlock mbox
if( flock(MBOX, 8) == 0){
  close(MBOX);
  &errExit(" * cant unlock $MAILBOX\n");
}

close(MBOX);










&cleanTmpDir();
print(LOG "------------------------------------------------\n");
close(LOG);

&sendXMPP("ricmailer5: отчет по работе", $xmpp_msg) if ($xmpp_msg);

if ($xmpp_connected ==1)
{
	$xmpp_connection->Disconnect();
	$xmpp_connected = 0;
}




exit($success);





