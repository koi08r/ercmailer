#!/usr/bin/perl


use locale;
use POSIX qw(locale_h);
setlocale(LC_ALL,"ru_RU.UTF-8");
use utf8;
use encoding 'utf8';

my $input = ""; $input .= $_ while (<>);
& Parse($input);

sub Parse()
{
	my $text = shift;
	# Encode::_utf8_on($text);

	my $ls_ = "";
	my $gv_ = "";

	print "Input: '$text'\n";

	# Удаляем все кроме пробелов, букв, цифр, точек и запятых.
	$text =~ s/(?:[^\w,\.\d]+|[\n\r\s]+)/\x20/g;
	# $text =~ s/(?:^\s+|\s+$)//g;
        # $text =~ s/Ё/Е/gi;
	# $text = "\U$text";

	print "Pre process: '$text'\n";

	# my $regex = 'ЛС\s*(\d{6})';
	# Encode::_utf8_on($regex);
	$ls_ = $1 if ($text =~ m/ЛС\s*(\d{6})/i);
	$gv_ = $1 if ($text =~ m/ГВ\s*(\d+(?:[.,]\d+)?)/i);

	print "Found ls: $ls_\n","Found gv: $gv_\n";

return 1;
}

