#!/usr/bin/perl

use MIME::Entity;
use SendMail;
use utf8;

my $entity = MIME::Entity->build (
    From        => 'fetchmail.erc@gmail.com',
    To          => 'fetchmail.erc@gmail.com',
    Subject     => 'Тестовое MIME сообщение',
    Charset     => 'UTF-8',
    Data        => "Привет ;)",
    Encoding    => "base64",
    'X-Mailer'  => "P-Mailer",
);

$entity->attach (
    Type        => 'text/plain',
    Charset    => 'UTF-8',
    Data        => "Вложение txt.",
    Disposition => "attachment",
    Filename    => "file.txt",
    Encoding     => "base64"
);

my $sm = new SendMail;
die "Can't send\n" unless ($sm->send(
	"fetchmail.erc\@gmail.com",
	"Тестовое MIME сообщение",
	"Привет ;)"
));
exit 0;

die "Usage: $0 <print|send>" unless (@ARGV > 0);

if ($ARGV[0] eq 'print') {
    $entity->print(\*STDOUT);
} elsif ($ARGV[0] eq 'send') {
    $entity->send (
	'smtps',
	Server => "smtp.gmail.com",
	Auth => [ "fetchmail.erc\@gmail.com", "Hfcxtn_51" ],
	Port => 465
	) or die "Can't send\n";
} else {
    die "Usage: $0 <print|send>";
}
