#!/usr/bin/perl

use DBI;
use utf8;

& do();

sub do {
	my $self = shift;

	my $dbh = DBI->connect("DBI:XBase:/home/koi8-r/prog/perl/ercmailer/out/counters/", undef, undef, { RaiseError => 1 })
		or die $DBI::errstr;

	my @names = $dbh->tables('', '', '%', 'TABLE');
	my $e = 0;
	foreach (@names) {
		print "$_\n";
		$e = 1;
	}

	unless ($e) {
		$dbh->do(q/CREATE TABLE counters (login CHAR, pass CHAR)/)
			or die $dbh->errstr();
	}

	my $sth = $dbh->prepare(q/INSERT INTO counters (login, pass) VALUES ('koi8-r','pass')/)
		or die $dbh->errstr();
	$sth->execute
		or die $sth->errstr();
	$sth->finish;

return 1;
}

1;
