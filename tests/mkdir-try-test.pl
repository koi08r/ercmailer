#!/usr/bin/perl

use File::Path qw/make_path/;

#my $dir = "/mnt/titan-tmp";
my $dir = "/tmp/mail/";

print "Start\n"; {

	eval {
		die "Err\n" unless (&mkDir ($dir));
	};

	print "R: $@\n";

} print "End\n";


sub try(&) {
	# eval {$_[0]->()}
	print "$_[0]\n";
}

try {
	print "Hello, World!\n";
};


sub mkDir {
	my $path = shift;

	unless (-e $path) {
		my $oldumask = umask 0022;
		make_path($path, {error => \my $err}) or return 0;
		umask $oldumask;
	}

return 1;
}
