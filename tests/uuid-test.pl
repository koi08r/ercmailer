#!/usr/bin/perl

use Data::UUID;

my %h = ();

my $duuid = new Data::UUID;
my $uuid1 = $duuid->create();
my $uuid2 = $duuid->create_from_name("ns", "name");


$h{ $duuid->to_string($uuid1) } = "First transact";
$h{ $duuid->to_string($uuid2) } = "Second transact";

while (my($uuid,$transact) = each %h) {
    print "Uuid: '$uuid', transact: '$transact'\n";
}
