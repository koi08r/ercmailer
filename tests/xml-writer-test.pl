#!/usr/bin/perl

use XML::LibXML;


my $fname = "xml-writer-test.xml";
my ($doc,$root);

eval {
# $parser->clean_namespaces();
unless (-e $fname) {
	$doc = XML::LibXML::Document->new('1.0', 'utf-8');
	$root = $doc->createElementNS('me@koi8-r.name', 'root');
	$root->setNamespace('admin@koi8-r.name', "admin",0);
	$root->setNamespace('www@koi8-r.name', "www",0);
	$root->setAttribute('email'=> 'me@koi8-r.name');
	$root->setNamespace('koi8-r@koi8-r.name', "me",0);
	# $root->setAttribute('xmlns:me','koi8-r@koi8-r.name');
	$doc->setDocumentElement($root);
} else {
	$doc = XML::LibXML->load_xml(location => $fname, keep_blanks => 0);
	$root = $doc->getDocumentElement();
}
};

if ($@) { die "Eval fail\n"; }

{
# my $sc_elem = $doc->createElement("me:sc");
my $sc_elem = $doc->createElement("sc");
$sc_elem->setNamespace('koi8-r@koi8-r.name', "me");
$sc_elem->setAttribute("src","sms");
my $ls_elem = $doc->createElement("ls");
my $gv_elem = $doc->createElement("gv");
$ls_elem->appendTextNode("822123");
$gv_elem->appendTextNode("33.1");
$sc_elem->appendChild($ls_elem);
$sc_elem->appendChild($gv_elem);
$root->appendChild($sc_elem);
}

{
my $sc_elem = $doc->createElement("sc");
$sc_elem->setAttribute("src","mail");
my $ls_elem = $doc->createElement("ls");
my $gv_elem = $doc->createElement("gv");
$ls_elem->appendTextNode("822777");
$gv_elem->appendTextNode("44.8");
$sc_elem->appendChild($ls_elem);
$sc_elem->appendChild($gv_elem);
$root->appendChild($sc_elem);
}

print $doc->toString(2);
# $doc->toFile($fname,2);
# $doc->toFH(STDOUT,2);


@root_attr = $root->attributes();
foreach $attr (@root_attr) {
	if(ref($attr) eq 'XML::LibXML::Attr') {
		print "+------------------------\n",
			" Name: "          ,$attr->nodeName, "\n",
			" Value: "         ,$attr->getValue, "\n",
			"+------------------------\n";
	}
	elsif(ref($attr) eq 'XML::LibXML::Namespace') {
		print "+------------------------\n",
			" Name: "          ,$attr->nodeName, "\n",
			" Value: "         ,$attr->getValue, "\n",
			" Local name: "    ,$attr->getLocalName, "\n",
			" Namespace URI: " ,$attr->getNamespaceURI, "\n",
			" Prefix: "        ,$attr->getPrefix, "\n",
			"+------------------------\n";
		if ($attr->nodeName eq 'xmlns' && !$attr->getLocalName) {
			print "+--- Found root default ns: ", $attr->getValue, "\n";
		}
	}
}


sub getDefaultNs {
	my @attr = shift->attributes();
	foreach (@root_attr) {
		if(ref eq 'XML::LibXML::Namespace') {
			return $_->getValue if ($_->nodeName eq 'xmlns' && !$_->getLocalName);
		}
	}
return undef;
}

print "Test getDefaultNs() on root node ... ";
if (my $ns = &getDefaultNs($root)) {
	print "'$ns'.\n";
} else {
	print "not found.\n";
}

exit 0;

my ($xquery,@nodes);
my $xc = XML::LibXML::XPathContext->new($doc);
$xc->registerNs('ns', 'me@koi8-r.name');

# /*[local-name() = 'elem' and namespace-uri() = 'me@koi8-r.name']
# /root[namespace-uri()="me@koi8-r.name"]

$xquery = '/ns:root/me:sc[@src="sms"]/ns:ls/text()';
# @nodes = $doc->findnodes($xquery);
@nodes = $xc->findnodes($xquery);
foreach $node (@nodes) {
	print $node->nodeName,"=",$node->nodeValue,"\n";
}

$xquery = '/ns:root/me:sc/ls';
@nodes = $xc->findnodes($xquery);
foreach $node (@nodes) {
	print $node->nodeName,"=",$node->textContent,"\n";
}
