#!/usr/bin/perl

use XML::LibXML;

debug ("Argv size: ".int(@ARGV)."\n");

my $fname = "-";
my $query = "";
my ($doc,$root);

for (my $i=0 ; $i<(@ARGV) ; $i++) {
	if ($ARGV[$i] eq '-f') {
		&usage unless ($i<@ARGV-1);
		$fname = $ARGV[$i+1];
	}
	if ($ARGV[$i] eq '-q') {
		&usage unless ($i<@ARGV-1);
		$query = $ARGV[$i+1];
	}
}

debug ("Reading from : '$fname'\n");

eval {
	# STDIN
	if ($fname eq "-") {
		$doc = XML::LibXML->load_xml(IO => STDIN, keep_blanks => 0);
	} else {
		$doc = XML::LibXML->load_xml(location => $fname, keep_blanks => 0);
	}
	$root = $doc->getDocumentElement();
};

if ($@) { die "Load xml fail\n"; }


my $xc = XML::LibXML::XPathContext->new($doc);
$xc->registerNs('ns', 'me@koi8-r.name');




unless ($query) {
	&findAll;
} else {
	my @pairs = split /&/, $query;
	my ($ls,$src) = ("","");

	foreach $a (@pairs) {
		$ls = $1 if ($a =~ /ls=(\d+)/);
		$src = $1 if ($a =~ /src=([\d\w\@\.\-]+)/);
	}

	debug("Ls: '$ls', Src: '$src'\n");

	if($ls && $src) {
		&findLsSrc($ls,$src);
	} elsif ($src) {
		&findSrc($src);
	} elsif ($ls) {
		&findLs($ls);
	} else {
		# die "Unknown query syntax\n";
	}
}

# =====================>

sub findAll {
	debug("findAll\n");
	debug("+------------------------\n");
	my $xquery = "/ns:root/ns:sc/ns:ls";
	@nodes = $xc->findnodes($xquery);
	foreach $node (@nodes) {
		&printScNode($node->parentNode);
	}
}

sub findLsSrc {
	my ($ls,$src) = @_;
	debug("findLsSrc=$ls,$src\n");
	debug("+------------------------\n");
	my $xquery = "/ns:root/ns:sc[\@src=\"$src\"]/ns:ls[text()=$ls]";
	@nodes = $xc->findnodes($xquery);
	foreach $node (@nodes) {
		&printScNode($node->parentNode);
	}
}

sub findSrc {
	my $src = shift;
	debug("findSrc=$src\n");
	debug("+------------------------\n");
	my $xquery = "/ns:root/ns:sc[\@src=\"$src\"]";
	@nodes = $xc->findnodes($xquery);
	foreach $node (@nodes) {
		&printScNode($node);
	}
}

sub findLs {
	my $ls = shift;
	debug("findLs=$ls\n");
	debug("+------------------------\n");
	my $xquery = "/ns:root/ns:sc/ns:ls[text()=$ls]";
	@nodes = $xc->findnodes($xquery);
	foreach $node (@nodes) {
		&printScNode($node->parentNode);
	}
}

# =====================>

sub printScNode {
	my $sc = shift;
	foreach $attr ($sc->attributes) {
		if(ref($attr) eq 'XML::LibXML::Attr') {
				print $attr->nodeName, " = " ,$attr->getValue, "\n";
		}
	}
	foreach $child ($sc->childNodes) {
		print $child->nodeName, " = " ,$child->textContent, "\n";
	}
	print "+------------------------\n";
}





sub debug {
	print shift if ($ENV{DEBUG}||$ENV{debug});
}

sub usage {
	die "Usage: $0 [-f <filename>] [-q query]\n";
}
